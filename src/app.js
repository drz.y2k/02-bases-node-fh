// importamos nuestra funcion getPokemonById
const getPokemonById = require('./js-foundation/06-promises');

// ejecutamos nuestra función, enviamos un id del pokemon a consultar
//y agregamos un callback que recibe el nombre del pokemon
//que corresponde con el id que enviamos y lo imprime en consola

//esta funcion retorna una promesa, porque retorna un fetch
//así que inmediatamente podemos imprimir en consola la respuesta
//retornada que es el nombre del pokemon que obtenemos de la API
//en caso de error se ejecutaria el catch, y el finally se ejecuta siempre
getPokemonById(4)
	.then(pokemon => console.log(pokemon))
	.catch(err => console.log(err.message+' desde el catch'))
	.finally(() => console.log('finalizado'));



// console.log(info);

// setTimeout(() => {
// 	console.log(info);
// }, 2000);