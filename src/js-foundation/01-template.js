// definimos una constante que será una plantilla de correo

const emailTemplate = `
	<div>
		<h1>Hi, {{name}}</h1>
		<p>Thank you for your order.</p>
	</div>
`;

//Como nota importante, si tenemos una función que se ejecuta en nuestro archivo, esta misma
//se va a ejecutar al momento de que nosotros importemos este archivo
// console.log(emailTemplate);

//la exportación por defecto en node, usa commonjs, por lo tanto para exportar usamos
//este código, es recomendable exportar como objeto para poder deestructurar donde se importe
module.exports = {
	emailTemplate
};