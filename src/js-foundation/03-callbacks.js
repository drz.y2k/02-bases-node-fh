//definimos un arreglo de objetos sobre el cual haremos una búsqueda
const users = [
	{ id: 1, name: 'John Doe' },
	{ id: 2, name: 'Jane Doe' },
];

//creamos la función de busqueda, la cual tambien recibirá un callback,
//es decir una función que ejecutaremos desde esta otra función
function getUserById(id, callback) {
	const user = users.find(function (user) {
		return user.id === id;
	})
	// usamos find para obtener el primer elemento que cumpla con la condición
	// en este caso que coincida con el id que recibimos

	// console.log({user});
	//si no se encuentra usuario, ejecutamos nuestro callback que recibe como
	//primer parámetro un error
	if (!user) {
		return callback(`User not found with id ${id}`);
	}
	//si se encuentra el usuario, enviamos error nulo, y enviamos como segundo parámetro el usuario encontrado
	callback(null, user);

}

// getUserById(1);
//exportamos nuestra función
module.exports = {
	getUserById
};