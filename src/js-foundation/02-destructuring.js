// process es un proceso de node que contiene información sobre la ejecución de node
//normalmente se usa para acceder a variables de entorno como en este caso
//donde leemos la variable puerto y si no existe usamos el puerto 3000
console.log(process.env.PORT ?? 3000);

//podemos desestructurar otras variables de nuestro process.env, no necesariamente
//las que nosotros definimos
const { SHELL, HOMEBREW_PREFIX, npm_lifecycle_script } = process.env;
//podemos ver mas facilmente estas variables usando un console.table
console.table({ SHELL, HOMEBREW_PREFIX, npm_lifecycle_script });

//tambien los arreglos s epueden desestructurar como es el caso aquí
const characters = ['Flash', 'Superman','Green Lantern', 'Batman'];

//podemos asignar variables de nombre guion bajo _ que es valido, en este 
//caso lo usamos para ignorar el primer elemento del arreglo
// const [_, __, batman] = characters;
//o podriamos simplemente no declararle ningun nombre y usar comas
//y declarar nombre solo al tercer elemento, que es el que queremos obtener
//para imprimirlo
const [,,,batman] = characters;

console.log(batman);