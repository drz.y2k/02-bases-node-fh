
//creamos nuestra funcion que consulta pokemon en base a un id, recibimos
//el id del pokemon a buscar, y un callback para ejecutar al finalizar la consulta
// const getPokemonByID = (id) => {
// 	//creamos la url de la api a consultar
// 	const url = `https://pokeapi.co/api/v2/pokemon/${id}`;

// 	//usamos fetch para hacer la consulta a la api

// 	//otra forma en la que podemos encadenar promesas que no sea anidandolas
// 	//es usando varios then en secuencia, en esta función estamos retornando
// 	//una promesa al retornar un fetch, para obtener los datos de fetch como json
// 	//debemos crear una nueva promesa que haga eso, una vez que tenemos los datos podemos
// 	//como en este ejemplo, retornar el nombre del pokemon
// 	//agregamos una promesa intermedia que lance un error
// 	//para probar que funcione correctamente el catch, aunque podriamos
// 	//quitar esta línea y el comportamiento sería el mismo
// 	return fetch(url)
// 		.then(res => res.json())
// 		// .then(()=>{throw new Error('pokemon no existe')})
// 		.then(pokemon => pokemon.name)

// }

// convertimos nuestra funcion a una función async,
//de este modo js sabrá que es una función que incluye una promesa

//importamos nuestro adaptador de peticiones http,
//y reemplazamos la implementación de este codigo donde usamos fetch
//por usar esa implementación de nuestro adaptador

const { http } = require('../plugins');

const getPokemonByID = async (id) => {
	const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
	//usamos await para esperar la respuesta de la api,
	//esta es la promesa de esta función
	// const res = await fetch(url);
	//usamos await para convertir nuestra respuesta del fetch
	//a json, esta es otra promesa de esta función
	// const pokemon = await res.json();

	//usamos nuestro adaptador, que ya hace la petición
	//y retorna la respuesta como json, por lo cual podemos asignarlo
	//directamente a una variable
	const pokemon = await http.get(url);



	//retornamos el nombre del pokemon
	return pokemon.name;

	//en caso de querer retornar un error que sea capturado por el catch
	//podemos usar throw new Error('mensaje de error')
	//aunque lo más normal es usar try/catch en funciones async para manejar erroes
	//recordar que throw y return no pueden ir al mismo nivel
	//porque ambos detienen la ejecución del código siguiente

	// throw new Error('pokemon no existe');
}

//exportación por defecto
module.exports = getPokemonByID;
