// una factory function es una funcion que construye y retorna otra función, como en este ejemplo de abajo

// const makeBuildPerson = () => {
// 	return () => {
// 		return {
// 			name: 'John',
// 			age: 20
// 		}
// 	}
// }

// importamos las librerias instaladas
// const { v4: uuidv4 } = require('uuid');
// const getAge = require('get-age');

// const { getAge } = require('../plugins/get-age.plugin');
// const { getUUID } = require('../plugins/get-id.plugin');

// importamos las funciones de nuestros plugins get-age y get-id
//aqui nos apoyamos del archivo de barril para importar desde una unica ruta y cambiamos en la funcion
//constructora por estas funciones
// const { getAge, getUUID } = require('../plugins');


//recibimos las implementaciones de getAge y getUUID como parametros
//para poder usarlas dentro de nuestra función y poder asignarlas en el momento
//que queramos llamar nuestra función constructora
const buildMakePerson = ({ getUUID, getAge }) => {
	//retornamos una funcion desde nuestra función, que a su vez recibe y retorna un objeto
	return ({ name, birthDate }) => {
		return {
			id: getUUID(),
			name,
			birthDate,
			age: getAge(birthDate)
		}
	}
}

// creamos una funcion que crea y retorna un objeto persona en base a un nombre y una fecha de nacimiento
// que recibe como parametro dentro de un objeto
//cambiamos la implementacion de la edad y el id para usar librerias instaladas
//esto crea acoplamiento de nuestro código pues si un día decidimos cambiar la libreria
//debemos cambiar el código en todos los lugares donde se usa


// //creamos un objeto que contiene el nombre y la fecha de nacimiento a usar en nuestra función
// const obj = {
// 	name: 'John',
// 	birthDate: '1985-10-21'
// }

// //ejecutamos la función y guardamos el resultado en una constante
// const john = buildPerson(obj);

// //imprimimos el resultado
// console.log(john);

module.exports = {
	// buildPerson
	buildMakePerson
}