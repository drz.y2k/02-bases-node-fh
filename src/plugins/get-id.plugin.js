//usamos patrón adaptador y encapsulamos el uso de la libreria uuid en este
//archivo, desde aqui exportamos la funcion que hará uso del uuid y usaremos
//en los demás archivos para facilitar el cambio en caso
//de que se decida usar otra libreria
const { v4: uuidv4 } = require('uuid');

const getUUID = () => {
	return uuidv4();
};

module.exports = {
	getUUID
};