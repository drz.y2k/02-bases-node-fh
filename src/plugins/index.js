// creamos archivo de barril para exportar todas las funciones de nuestros plugins

const { getUUID } = require('./get-id.plugin');
const { getAge } = require('./get-age.plugin');
const { http } = require('./http-client.plugin');

module.exports = {
	getUUID,
	getAge,
	http
};