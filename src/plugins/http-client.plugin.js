// implementamos patron adaptador,
//al envolver nuestra petición eun un atributo de un objeto, en este caso el
//get que recibe una url y retorna la respuesta de hacer el fetch a esa url
//y convertirla a json, esto nos ayuda para cuando necesitemos cambiar el fetch
//por usar por ejemplo axios, solo lo cambiamos desde este archivo

//instalamos axios y lo importamos usando la sintáxis de commonjs
const axios = require('axios');


//podemos hacer una factory funciton para recibir headers en nuestro get, por ejemplo para la autenticación
//este sería una estructura básica de como quedaríá
// const buildHttpClient = (headers) => {
// 	return {
// 		get: async (url) => {axios.get(url,headers); },
// 		post: async (url, body) => { },
// 		put: async (url, body) => { },
// 		delete: async (url) => { },
// 	}
// }

//implementamos axios para hacer nuestra petición HTTP reemplazando el fetch
const httpClientPlugin = {
	get: async (url) => {
		// const res = await fetch(url);
		// return await res.json();
		//desestructuramos la data de la respuesta de axios
		//y la retornamos
		const { data } = await axios.get(url);
		// console.log(data);
		return data;
	},
	post: async (url, body) => { },

	put: async (url, body) => { },
	delete: async (url) => { },
};

module.exports = { http: httpClientPlugin };