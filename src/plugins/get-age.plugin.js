// creamos nuestro patron adaptador en este archivo
// esta será la unica importacion que se hará de la libreria get-age
//en todos los lados donde se use se hará referencia a esta función que será exportada
//para facilitar cambios en caso de que se requiera cambiar la libreria, ya que 
//solo se cambiaría en este archivo
const getAgePlugin = require('get-age');


const getAge = (birthDate) => {
	if (!birthDate) return new Error('birthDate is required');

	return getAgePlugin(birthDate);
}

module.exports = {
	getAge
}